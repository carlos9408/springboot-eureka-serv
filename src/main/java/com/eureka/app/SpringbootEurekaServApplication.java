package com.eureka.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class SpringbootEurekaServApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootEurekaServApplication.class, args);
	}

}
